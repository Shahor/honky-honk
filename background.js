const platform = chrome || browser

chrome.runtime.onMessage.addListener(({ file }) => {
    const audio = new Audio()
    audio.addEventListener("canplay", () => {
        audio.play()
    })
    audio.src = platform.extension.getURL(`sounds/${file}.mp3`)
})
