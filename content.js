const platform = chrome || browser

function random(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min)
}

document.addEventListener("click", event => {
    const { target } = event

    if (random(0, 100) > 20) {
        return
    }

    if (target.tagName === "A") {
        platform.runtime.sendMessage({ file: random(0, 9) })
    }
})
